import os, requests, json
from os.path import exists
with open('list.txt') as file:
  for line in file:
    url = line.replace('\n','')
    protocol = "https://" if line.startswith('https://') else "http://"
    path = line.replace(protocol+'assets.thestandnews.com/','').replace(line.split('/').pop(),'')
    try:
      if not exists(path+line.split('/').pop().replace('\n','')):
        if not exists(path) and len(path) != 0:
          os.system("mkdir -p "+path)
        x = json.loads(requests.get('https://archive.org/wayback/available?url='+line.replace(protocol,'')).text)
        if x["archived_snapshots"] != {}:
          os.system("curl "+x["archived_snapshots"]["closest"]["url"].replace('/'+protocol+'assets.thestandnews.com', 'im_/'+protocol+'assets.thestandnews.com')+" -o "+line.replace(protocol+'assets.thestandnews.com/',''))
    except:
      print(line)
      with open('failed.txt','a') as file1:
        file1.write(line)
