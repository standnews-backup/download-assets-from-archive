import os
threads = int(input("threads: "))
os.system('mkdir workers')
start = 1
n_lines = int(550000/threads)
for i in range(threads):
  with open('list.txt') as file:
    for line in file.readlines()[start:(start+n_lines)]:
      with open("workers/list"+str(i)+".txt", "a") as newfile:
        newfile.write(line)
  cmd = "cp dl.py workers/dl"+str(i)+".py && sed -i \"s/list.txt/workers\/list"+str(i)+".txt/g\" workers/dl"+str(i)+".py"
  print(cmd)
  os.system(cmd)
  start += n_lines
  start = 545864 - n_lines if start > 545864 - n_lines else start
cmd = "python3 workers/dl0.py"
for i in range(0,threads):
  cmd += " & python3 workers/dl"+str(i)+".py"
os.system(cmd)
