# About
Fetch all stand news image files from the internet archive.
# Run
```
python3 multi.py
```
# Note
The amount of images is enormous. You need at least 50gb of storage.
